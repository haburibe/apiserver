API server sample written in go

```
$ cd apiserver
$ go build
$ ./apiserver &
$ http --form POST :8080 message="Hello, world"
$ http GET :8080
$ pkill apiserver 
```
