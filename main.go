package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type Message struct {
	Text      string    `json:"text"`
	CreatedAt time.Time `json:"created_at"`
}

var messages []Message

func Handler(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, r.URL)
	switch r.Method {
	case "GET":
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(messages)
	case "POST":
		r.ParseForm()
		message := Message{
			Text:      r.Form.Get("message"),
			CreatedAt: time.Now(),
		}
		messages = append(messages, message)

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(message)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func main() {
	messages = make([]Message, 0)
	http.HandleFunc("/", Handler)
	log.Println("Running on http://localhost:8080/")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
